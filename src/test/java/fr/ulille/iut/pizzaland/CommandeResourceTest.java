package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.CommandeAssociation;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.PizzaAssociation;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class CommandeResourceTest extends JerseyTest {
    private CommandeDao commandeDao;
    private PizzaDao pizzaDao;
    private IngredientDao ingredientDao;
  
    @Override
    protected Application configure() {
	    BDDFactory.setJdbiForTests();
		return new ApiV1();
  	}

	@Before
	public void setEnvUp() {
	    pizzaDao = BDDFactory.buildDao(PizzaDao.class);
	    commandeDao = BDDFactory.buildDao(CommandeDao.class);
	    ingredientDao = BDDFactory.buildDao(IngredientDao.class);
	    commandeDao.createCommandeAndAssociationTable();
	    pizzaDao.createPizzaAndAssociationTable();
	    ingredientDao.createTable();
	}
	
	@After
	public void tearEnvDown() throws Exception {
	   pizzaDao.dropTable();
	   commandeDao.dropTable();
	}
	
	@Test
	public void testGetExistingCommande() {
	
		Commande commande = new Commande();
		commande.setFirstName("Armand");
		commande.setName("Vidal");
	    long id = commandeDao.insert(commande.getFirstName(), commande.getName());
	    commande.setId(id);
	    System.out.println("FIRST NAME : "+commande.getFirstName());
	
	    Response response = target("/commandes/" + id).request().get();
	
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	    
	    Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
	    assertEquals(commande, result);
	}
	
	@Test
	public void testGetEmptyList() {
	// La méthode target() permet de préparer une requête sur une URI.
	// La classe Response permet de traiter la réponse HTTP reçue.
	    Response response = target("/commandes").request().get();

	// On vérifie le code de la réponse (200 = OK)
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		  
	// On vérifie la valeur retournée (liste vide)
	// L'entité (readEntity() correspond au corps de la réponse HTTP.
	// La classe javax.ws.rs.core.GenericType<T> permet de définir le type
	// de la réponse lue quand on a un type complexe (typiquement une liste).
	List<CommandeDto> commandes;
	    commandes = response.readEntity(new GenericType<List<CommandeDto>>(){});

	    assertEquals(0, commandes.size());

	}
	
	@Test
	public void testGetNotExistingPorte() {
	  Response response = target("/portes/125").request().get();
	  assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testCreateCommande() {
	    CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
	    commandeCreateDto.setFirstName("Armand");
	    commandeCreateDto.setName("Vidal");
 
	    //commandeDao.insert(commandeCreateDto.getFirstName(), commandeCreateDto.getName());

	    Response response = target("/commandes")
	            .request()
	            .post(Entity.json(commandeCreateDto));

	    // On vérifie le code de status à 201
	    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	    CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

	    // On vérifie que le champ d'entête Location correspond à
	    // l'URI de la nouvelle entité
	    assertEquals(target("/commandes/" +
			returnedEntity.getId()).getUri(), response.getLocation());
		
		// On vérifie que le nom correspond
	    assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
	    assertEquals(returnedEntity.getFirstName(), commandeCreateDto.getFirstName());
	}
	
	@Test
	public void testCreateCommandeWithPizzas() {
	    CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
	    commandeCreateDto.setFirstName("Arturo");
	    commandeCreateDto.setName("Armani");
	    List<Pizza> pizzas = new ArrayList<Pizza>();
	    
	    Pizza pizza = pizzaDao.findByName("orientale");
	    if(pizza == null) {
	    	long id = pizzaDao.insert("orientale");
	    	pizzas.add(new Pizza(id, "orientale", null));
	    }
	    else pizzas.add(pizza);
	    
	    Pizza pizza2 = pizzaDao.findByName("experimental");
	    if(pizza2 == null) {
	    	long id2 = pizzaDao.insert("experimental");
	    	List<Ingredient> ingredients = new ArrayList<Ingredient>();
	    	Ingredient ingredient1 = ingredientDao.findByName("haricot");
		    if(ingredient1 == null) {
		    	long id = ingredientDao.insert("haricot");
		    	ingredients.add(new Ingredient(id, "haricot"));
		    }
		    else ingredients.add(ingredient1);
		    
		    Ingredient ingredient2 = ingredientDao.findByName("petit pois");
		    if(ingredient2 == null) {
		    	long id = ingredientDao.insert("petit pois");
		    	ingredients.add(new Ingredient(id, "petit pois"));
		    }
		    else ingredients.add(ingredient2);
	    	
	    	pizzas.add(new Pizza(id2, "experimental", ingredients));
	    }
	    else pizzas.add(pizza2);
	    
	    commandeCreateDto.setPizzas(pizzas);

	    Response response = target("/commandes")
	            .request()
	            .post(Entity.json(commandeCreateDto));

	    // On vérifie le code de status à 201
	    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	    CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

	    // On vérifie que le champ d'entête Location correspond à
	    // l'URI de la nouvelle entité
	    assertEquals(target("/commandes/" +
			returnedEntity.getId()).getUri(), response.getLocation());
		
		// On vérifie que le nom correspond
	    assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
	    assertEquals(returnedEntity.getFirstName(), commandeCreateDto.getFirstName());
	    assertEquals(pizzas, commandeCreateDto.getPizzas());
	}
	
	@Test
	public void testCreateSameCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setFirstName("Armand");
	    commandeCreateDto.setName("Vidal");
	    commandeDao.insert(commandeCreateDto.getFirstName(), commandeCreateDto.getName());

	    Response response = target("/commandes")
	            .request()
	            .post(Entity.json(commandeCreateDto));

	    assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommandeWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

	    Response response = target("/commandes")
	            .request()
	            .post(Entity.json(commandeCreateDto));

	    assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingCommande() {
	    Commande commande = new Commande();
	    commande.setFirstName("Armand");
	    commande.setName("Vidal");
	    long id = commandeDao.insert(commande.getFirstName(), commande.getName());
	    commande.setId(id);

	    Response response = target("/commandes/" + id).request().delete();

	    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	    Pizza result = pizzaDao.findById(id);
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingCommande() {
	    Response response = target("/commandes/125").request().delete();
	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
		response.getStatus());
	}
	
	@Test
	public void testGetCommandeName() {
		Commande commande = new Commande();
		commande.setFirstName("Armand");
	    commande.setName("Vidal");
	    long id = commandeDao.insert(commande.getFirstName(), commande.getName());
	    commande.setId(id);

	    Response response = target("commandes/" + id + "/name").request().get();

	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    assertEquals("Vidal", response.readEntity(String.class));
	}
	
	@Test
	public void testGetCommandePizza() {
		commandeDao.dropTable();
		commandeDao.createCommandeAndAssociationTable();
		pizzaDao.dropTable();
		pizzaDao.createPizzaAndAssociationTable();
		ingredientDao.dropTable();
	    ingredientDao.createTable();
		Commande commande = new Commande();
		commande.setFirstName("Armand");
	    commande.setName("Vidal");
		List<Pizza> initPizzas = new ArrayList<Pizza>();
		long id1 = pizzaDao.insert("bolognaise");
		long id2 = pizzaDao.insert("creme");
		//System.out.println("ID 1 : "+id1+" ID2 : "+id2);
		initPizzas.add(new Pizza(id1, "bolognaise", new ArrayList<Ingredient>()));
		
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		Ingredient ingredient1 = ingredientDao.findByName("sauce bolognaise");
	    if(ingredient1 == null) {
	    	long id = ingredientDao.insert("sauce bolognaise");
	    	ingredients.add(new Ingredient(id, "sauce bolognaise"));
	    }
	    else ingredients.add(ingredient1);
	    //System.out.println("GET 0 : "+initPizzas.get(0));
	    initPizzas.get(0).setIngredients(ingredients);
	    
		initPizzas.add(new Pizza(id2, "creme", new ArrayList<Ingredient>()));
		for(Ingredient ingredient: initPizzas.get(0).getIngredients()) {
	    	//System.out.println("L'INGREDIENT :"+ingredient.getName()+" ID : "+ingredient.getId());
	    	PizzaAssociation pa = pizzaDao.findAssociationByIds(initPizzas.get(0).getId(), ingredient.getId());
	    	//System.out.println("LA PA : "+pa.toString());
    		if(pa == null) System.out.println("ID INSERTION INGREDIENT : "+pizzaDao.insertIngredient(initPizzas.get(0).getId(), ingredient.getId()));
	    }
		//System.out.println(initPizzas.toString());
		commande.setPizzas(initPizzas);
		//System.out.println(commande.getPizzas());
		//System.out.flush();
	    long id = commandeDao.insert(commande.getFirstName(), commande.getName());
	    for(Pizza pizza: commande.getPizzas()) {
	    	CommandeAssociation pa = commandeDao.findAssociationByIds(id, pizza.getId());
    		if(pa == null) commandeDao.insertPizza(id, pizza.getId());
	    }
	    
	    List<CommandeAssociation> assos = commandeDao.findAssociationByCommande(id);
	    List<Pizza> pizzasTest = new ArrayList<Pizza>();
	    for(CommandeAssociation asso: assos) {
	    	Pizza pizza = pizzaDao.findById(asso.getIdPizza());
	    	if(pizza != null) pizzasTest.add(pizza);
	    }
	    
	    Response response = target("commandes/" + id + "/pizzas").request().get();

	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	    
	    //List<Pizza> pizzasTest;
	   // pizzasTest = response.readEntity(new GenericType<List<Pizza>>(){});
	    for(Pizza laPizza: pizzasTest) {
	    	List<Ingredient> pizzaIngredients = new ArrayList<Ingredient>();
	    	List<PizzaAssociation> pas = pizzaDao.findAssociationByPizza(laPizza.getId());
	    	for(PizzaAssociation asso: pas) {
	    		System.out.println("ASSO :"+asso.getIdPizza()+","+asso.getIdIngredient());
		    	Ingredient ingredient = ingredientDao.findById(asso.getIdIngredient());
		    	if(ingredient != null) pizzaIngredients.add(ingredient);
		    	System.out.println("Id : "+asso.getIdIngredient()+" Ingredient : "+ingredient);
		    }
	    	//System.out.println("Pizza : "+laPizza.getName()+" Ingredients : "+pizzaIngredients);
	    	laPizza.setIngredients(pizzaIngredients);
	    }
	   // System.out.println("INIT:"+initPizzas);
	    //System.out.println("END :"+pizzasTest);
	    assertEquals(initPizzas, pizzasTest);
	    //LES ELEMENTS SONT IDENTIQUES MAIS CA RENVOIE FAUX
	}
}
