package fr.ulille.iut.pizzaland.resources;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.PizzaAssociation;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizzas")
public class PizzaResource {
	@Context
	public UriInfo uriInfo;

  private PizzaDao pizzas;
  private IngredientDao ingredientsDao;
  private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
  
  public PizzaResource() {
    pizzas = BDDFactory.buildDao(PizzaDao.class);
    ingredientsDao = BDDFactory.buildDao(IngredientDao.class);
    pizzas.createPizzaAndAssociationTable();
    //pizzas.createPizzaTable();
    //ingredientsDao.createTable();
  }
  
  @GET
  public List<PizzaDto> getAll() {
    LOGGER.info("PizzaResource:getAll");

    List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
    return l;
  }

  @GET
  @Path("{id}")
  public PizzaDto getOnePizza(@PathParam("id") long id) {
    LOGGER.info("getOnePizza(" + id + ")");
    try {
        Pizza pizza = pizzas.findById(id);
		return Pizza.toDto(pizza);
    }
    catch ( Exception e ) {
		// Cette exception générera une réponse avec une erreur 404
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }
  
  @POST
  public Response createPizza(PizzaCreateDto pizzaCreateDto) {
      Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
      if ( existing != null ) {
          throw new WebApplicationException(Response.Status.CONFLICT);
      }
      
      try {
          Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
          System.out.println("NOM PIZZA : "+pizza.getName());
          long id = pizzas.insert(pizza.getName());
          pizza.setId(id);
          if(pizza.getIngredients() != null) {
	          for(int i=0; i<pizza.getIngredients().size(); i++) {
	        	  //if
	        	  pizzas.insertIngredient(id, pizza.getIngredients().get(i).getId());
	          }
          }
          
          PizzaDto pizzaDto = Pizza.toDto(pizza);

          URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

          return Response.created(uri).entity(pizzaDto).build();
      }
      catch ( Exception e ) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
  }

  @DELETE
  @Path("{id}")
  public Response deletePizza(@PathParam("id") long id) {
  	if ( pizzas.findById(id) == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}

      pizzas.remove(id);
      pizzas.removeAssociationByPizza(id);

      return Response.status(Response.Status.ACCEPTED).build();
  }

  @GET
  @Path("{id}/name")
  public String getPizzaName(@PathParam("id") long id) {
      Pizza pizza = pizzas.findById(id);
  	if ( pizza == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}
          
  	return pizza.getName();
  }
  
  @GET
  @Path("{id}/ingredients")
  public List<Ingredient> getPizzaIngredients(@PathParam("id") long id) {
      Pizza pizza = pizzas.findById(id);
  	if ( pizza == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}
  	
  	List<PizzaAssociation> assos = pizzas.findAssociationByPizza(id);
    List<Ingredient> ingredients = new ArrayList<Ingredient>();
    for(PizzaAssociation asso: assos) {
    	Ingredient ingredient = ingredientsDao.findById(asso.getIdIngredient());
    	if(ingredient != null) ingredients.add(ingredient);
    }
          
  	return ingredients;
  }

  
}