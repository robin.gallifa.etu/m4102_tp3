package fr.ulille.iut.pizzaland.resources;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.CommandeAssociation;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

@Path("/commandes")
public class CommandeResource {
	@Context
	public UriInfo uriInfo;

  private CommandeDao commandes;
  private PizzaDao pizzaDao;
  private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
  
  public CommandeResource() {
    commandes = BDDFactory.buildDao(CommandeDao.class);
    pizzaDao = BDDFactory.buildDao(PizzaDao.class);
   // ingredientsDao = BDDFactory.buildDao(IngredientDao.class);
    commandes.createCommandeAndAssociationTable();
    //commandes.createCommandeTable();
    //ingredientsDao.createTable();
  }
  
  @GET
  public List<CommandeDto> getAll() {
    LOGGER.info("CommandeResource:getAll");

    List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
    return l;
  }

  @GET
  @Path("{id}")
  public CommandeDto getOneCommande(@PathParam("id") long id) {
    LOGGER.info("getOneCommande(" + id + ")");
    try {
        Commande commande = commandes.findById(id);
		return Commande.toDto(commande);
    }
    catch ( Exception e ) {
		// Cette exception générera une réponse avec une erreur 404
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }
  
  @POST
  public Response createCommande(CommandeCreateDto commandeCreateDto) {
      Commande existing = commandes.findByName(commandeCreateDto.getName());
      if ( existing != null ) {
          throw new WebApplicationException(Response.Status.CONFLICT);
      }
      
      try {
          Commande commande = Commande.fromCommandeCreateDto(commandeCreateDto);
          //System.out.println("NOM PIZZA : "+commande.getName());
          long id = commandes.insert(commande.getFirstName(), commande.getName());
          commande.setId(id);
          if(commande.getPizzas() != null) {
	          for(int i=0; i<commande.getPizzas().size(); i++) {
	        	  //if
	        	  //commandes.insertPizza(id, commande.getPizzas().get(i).getId());
	          }
          }
          
          CommandeDto commandeDto = Commande.toDto(commande);

          URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

          return Response.created(uri).entity(commandeDto).build();
      }
      catch ( Exception e ) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
  }

  @DELETE
  @Path("{id}")
  public Response deleteCommande(@PathParam("id") long id) {
  	if ( commandes.findById(id) == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}

      commandes.remove(id);
      commandes.removeAssociationByCommande(id);

      return Response.status(Response.Status.ACCEPTED).build();
  }

  @GET
  @Path("{id}/name")
  public String getCommandeName(@PathParam("id") long id) {
      Commande commande = commandes.findById(id);
  	if ( commande == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}
          
  	return commande.getName();
  }
  
 @GET
  @Path("{id}/pizzas")
  public List<Pizza> getCommandePizzas(@PathParam("id") long id) {
      Commande commande = commandes.findById(id);
  	if ( commande == null ) {
  		throw new WebApplicationException(Response.Status.NOT_FOUND);
  	}
  	
  	List<CommandeAssociation> assos = commandes.findAssociationByCommande(id);
    List<Pizza> pizzas = new ArrayList<Pizza>();
    for(CommandeAssociation asso: assos) {
    	Pizza pizza = pizzaDao.findById(asso.getIdPizza());
    	if(pizza != null) pizzas.add(pizza);
    }
          
  	return pizzas;
  }

  
}