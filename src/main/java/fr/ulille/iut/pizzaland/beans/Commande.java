package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private long id;
	private String firstName;
	private String name;
	private List<Pizza> pizzas;
	
	public Commande() {
		
	}
	
	public Commande(long id, String firstName, String name, List<Pizza> pizzas) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.name = name;
		this.pizzas = pizzas;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public static CommandeDto toDto(Commande i) {
	    CommandeDto dto = new CommandeDto();
	    dto.setId(i.getId());
	    dto.setFirstName(i.getFirstName());
	    dto.setName(i.getName());
	    dto.setPizzas(i.pizzas);

	    return dto;
	  }

	  public static Commande fromDto(CommandeDto commandeDto) {
	    Commande commande = new Commande();
	    commande.setId(commandeDto.getId());
	    commande.setFirstName(commandeDto.getFirstName());
	    commande.setName(commandeDto.getName());
	    commande.setPizzas(commandeDto.getPizzas());

	    return commande;
	  }
	  
	  public static CommandeCreateDto toCreateDto(Commande commande) {
		    CommandeCreateDto dto = new CommandeCreateDto();
		    dto.setFirstName(commande.getFirstName());
		    dto.setName(commande.getName());
		    dto.setPizzas(commande.getPizzas());
		    
		    return dto;
		}

		public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		    Commande commande = new Commande();
		    commande.setFirstName(dto.getFirstName());
		    commande.setName(dto.getName());
		    commande.setPizzas(dto.getPizzas());

		    return commande;
		}
		
		  @Override
		  public boolean equals(Object obj) {
		    if (this == obj)
		        return true;
		    if (obj == null)
		        return false;
		    if (getClass() != obj.getClass())
		        return false;
		    Commande other = (Commande) obj;
		    if (id != other.id)
		        return false;
		    if (name == null) {
		        if (other.name != null)
		            return false;
		    } else if (!name.equals(other.name))
		        return false;
		    if (firstName == null) {
		        if (other.firstName != null)
		            return false;
		    } else if (!firstName.equals(other.firstName))
		        return false;
		    return true;
		  }

		  @Override
		  public String toString() {
		    if(pizzas == null) {
		    	System.out.println("PASSAGE 1");
		    	return "Commande [id=" + id + ", firstName=" + firstName + ", name=" + name + "]";
		    }
		    System.out.println("PASSAGE2");
		    return "Commande [id=" + id + ", firstName=" + firstName + ", name=" + name + ", pizzas= ["+ pizzas.toString() +"]]";
		  }
	
	
}
