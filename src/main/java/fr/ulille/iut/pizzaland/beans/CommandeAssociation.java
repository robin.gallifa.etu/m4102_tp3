package fr.ulille.iut.pizzaland.beans;

public class CommandeAssociation {
	private long idCommande;
	private long idPizza;
	
	public CommandeAssociation() {
	  }
	
	public CommandeAssociation(long idCommande, long idPizza) {
		this.idCommande = idCommande;
		this.idPizza = idPizza;
	}
	
	public long getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(long idCommande) {
		this.idCommande = idCommande;
	}
	public long getIdPizza() {
		return idPizza;
	}
	public void setIdPizza(long idPizza) {
		this.idPizza = idPizza;
	}

	@Override
	public String toString() {
		return "CommandeAssociation [idCommande=" + idCommande + ", idPizza=" + idPizza + "]";
	}
	
	
}
