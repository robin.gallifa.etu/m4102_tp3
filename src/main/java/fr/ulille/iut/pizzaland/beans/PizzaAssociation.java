package fr.ulille.iut.pizzaland.beans;

public class PizzaAssociation {
	private long idPizza;
	private long idIngredient;
	
	public PizzaAssociation() {
	  }
	
	public PizzaAssociation(long idPizza, long idIngredient) {
		this.idPizza = idPizza;
		this.idIngredient = idIngredient;
	}
	
	public long getIdPizza() {
		return idPizza;
	}
	public void setIdPizza(long idPizza) {
		this.idPizza = idPizza;
	}
	public long getIdIngredient() {
		return idIngredient;
	}
	public void setIdIngredient(long idIngredient) {
		this.idIngredient = idIngredient;
	}

	@Override
	public String toString() {
		return "PizzaAssociation [idPizza=" + idPizza + ", idIngredient=" + idIngredient + "]";
	}
	
}
