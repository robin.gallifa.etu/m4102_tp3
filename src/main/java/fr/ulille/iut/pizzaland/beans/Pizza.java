package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	  private long id;
	  private String name;
	  private List<Ingredient> ingredients;

	  public Pizza() {
	  }

	  public Pizza(long id, String name, List<Ingredient> ingredients) {
	    this.id = id;
	    this.name = name;
	    this.ingredients = ingredients;
	  }

	  public void setId(long id) {
	    this.id = id;
	  }

	  public long getId() {
	    return id;
	  }

	  public String getName() {
	    return name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }
	  
	  public List<Ingredient> getIngredients() {
		return ingredients;
	  }

	  public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	  }

	public static PizzaDto toDto(Pizza i) {
	    PizzaDto dto = new PizzaDto();
	    dto.setId(i.getId());
	    dto.setName(i.getName());
	    dto.setIngredients(i.ingredients);

	    return dto;
	  }

	  public static Pizza fromDto(PizzaDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setId(dto.getId());
	    pizza.setName(dto.getName());
	    

	    return pizza;
	  }
	  
	  public static PizzaCreateDto toCreateDto(Pizza pizza) {
		    PizzaCreateDto dto = new PizzaCreateDto();
		    dto.setName(pizza.getName());
		    dto.setIngredients(pizza.getIngredients());
		    
		    return dto;
		}

		public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		    Pizza pizza = new Pizza();
		    pizza.setName(dto.getName());
		    pizza.setIngredients(dto.getIngredients());

		    return pizza;
		}

	  
	  @Override
	  public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Pizza other = (Pizza) obj;
	    if (id != other.id)
	        return false;
	    if (name == null) {
	        if (other.name != null)
	            return false;
	    } else if (!name.equals(other.name))
	        return false;
	    else {
	    	if(ingredients == null) {
	    		if(other.ingredients != null)
	    			return false;
	    	}
	    	else if(other.ingredients == null) {
	    		if(ingredients != null)
	    			return false;
	    	}
	    	else {
		    	if(other.getIngredients().size() != this.ingredients.size()) {
		    		return false;
		    	}
		    	for(int i=0; i<other.getIngredients().size() && i<this.ingredients.size(); i++) {
		    		if(!other.getIngredients().get(i).equals(this.ingredients.get(i))) {
		    			return false;
		    		}
		    	}
	    	}
	    }
	    return true;
	  }

	  @Override
	  public String toString() {
	    if(ingredients != null) return "Pizza [id=" + id + ", name=" + name + ", ingredients= ["+ingredients.toString()+"]]";
	    else return "Pizza [id=" + id + ", name=" + name + "]";
	  }
}
