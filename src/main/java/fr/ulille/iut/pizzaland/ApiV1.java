package fr.ulille.iut.pizzaland;

import org.glassfish.jersey.server.ResourceConfig;

import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.PizzaAssociation;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
	private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

  public ApiV1() {
    packages("fr.ulille.iut.pizzaland");

    String environment = System.getenv("PIZZAENV");

    if ( environment != null && environment.equals("withdb") ) {
	  LOGGER.info("Loading with database");
      Jsonb jsonb = JsonbBuilder.create();
      try {
	  	FileReader reader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
        List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>(){}.getClass().getGenericSuperclass());
            
        IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        ingredientDao.dropTable();
        ingredientDao.createTable();
        for ( Ingredient ingredient: ingredients) {
        	// System.out.println("Le NOM : "+ingredient.getName());
             ingredientDao.insert(ingredient.getName());              
        }
        
        PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);    
        pizzaDao.dropTable();
        pizzaDao.createPizzaTable();
        
	  	FileReader readerPizza = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
        List<Pizza> pizzas = JsonbBuilder.create().fromJson(readerPizza, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());
        
        for(Pizza pizza: pizzas) {
        	long idPizza = pizzaDao.insert(pizza.getName());
        	pizza.setId(idPizza);
        	for(Ingredient ingredient: pizza.getIngredients()) {
        		Ingredient ingredient1 = ingredientDao.findByName(ingredient.getName());
        		long id;
        	    if(ingredient1 == null) {
        	    	id = ingredientDao.insert(ingredient.getName());
        	    	ingredients.add(new Ingredient(id, ingredient.getName()));
        	    }
        	    else id = ingredient1.getId();
        	    
        	    PizzaAssociation pa = pizzaDao.findAssociationByIds(pizza.getId(), id);
        		if(pa == null) pizzaDao.insertIngredient(pizza.getId(), id);
        	}
        }
       
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
	    pizzaCreateDto.setName("orientale");
	    List<Ingredient> pizzaIngredients = new ArrayList<Ingredient>();
	    Ingredient ingredient1 = ingredientDao.findByName("mozzarella");
	    if(ingredient1 == null) {
	    	long id = ingredientDao.insert("mozzarella");
	    	ingredients.add(new Ingredient(id, "mozzarella"));
	    }
	    else ingredients.add(ingredient1);
        
	  } catch ( Exception ex ) {
		throw new IllegalStateException(ex);
      }
    } 
  }
}
