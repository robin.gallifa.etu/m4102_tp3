package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.PizzaAssociation;

public interface PizzaDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientAssociation (idPizza INTEGER NOT NULL, idIngredient INTEGER NOT NULL, FOREIGN KEY (idPizza) REFERENCES pizzas(id), FOREIGN KEY (idIngredient) REFERENCES ingredients(id), PRIMARY KEY(idPizza, idIngredient))")
	void createAssociationTable();
	
	@Transaction
	default void createPizzaAndAssociationTable() {
		createPizzaTable();
		createAssociationTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizzas; DROP TABLE IF EXISTS pizzaIngredientAssociation")
	void dropTable();
	
	@SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	
	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	  @RegisterBeanMapper(Pizza.class)
	  Pizza findById(long id);
	
	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);
	  
	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void remove(long id);
	  
		
	@SqlUpdate("INSERT INTO pizzaIngredientAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
	@GetGeneratedKeys
	long insertIngredient(long idPizza, long idIngredient);
		
	@SqlQuery("SELECT * FROM pizzaIngredientAssociation WHERE idPizza = :idPizza AND idIngredient = :idIngredient")
	@RegisterBeanMapper(PizzaAssociation.class)
	PizzaAssociation findAssociationByIds(long idPizza, long idIngredient);
	
	@SqlQuery("SELECT * FROM pizzaIngredientAssociation WHERE idPizza = :idPizza")
	@RegisterBeanMapper(PizzaAssociation.class)
	List<PizzaAssociation> findAssociationByPizza(long idPizza);
	
	@SqlUpdate("DELETE FROM pizzaIngredientAssociation WHERE idPizza = :idPizza")
	void removeAssociationByPizza(long idPizza);
}
