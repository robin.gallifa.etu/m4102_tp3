package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.PizzaAssociation;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.CommandeAssociation;

public interface CommandeDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id INTEGER PRIMARY KEY, firstName VARCHAR NOT NULL, name VARCHAR NOT NULL)")
	void createCommandeTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandePizzaAssociation (idCommande INTEGER NOT NULL, idPizza INTEGER NOT NULL, FOREIGN KEY (idCommande) REFERENCES commandes(id), FOREIGN KEY (idPizza) REFERENCES pizzas(id), PRIMARY KEY(idCommande, idPizza))")
	void createAssociationTable();
	
	@Transaction
	default void createCommandeAndAssociationTable() {
		createCommandeTable();
		createAssociationTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS commandes; DROP TABLE IF EXISTS commandePizzaAssociation")
	void dropTable();
	
	@SqlUpdate("INSERT INTO commandes (firstName, name) VALUES (:firstName, :name)")
	@GetGeneratedKeys
	long insert(String firstName, String name);

	@SqlQuery("SELECT * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();
	
	
	@SqlQuery("SELECT * FROM commandes WHERE id = :id")
	  @RegisterBeanMapper(Commande.class)
	  Commande findById(long id);
	
	@SqlQuery("SELECT * FROM commandes WHERE name = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(String name);
	  
	@SqlUpdate("DELETE FROM commandes WHERE id = :id")
	void remove(long id);
	
	@SqlUpdate("INSERT INTO commandePizzaAssociation (idCommande, idPizza) VALUES (:idCommande, :idPizza)")
	@GetGeneratedKeys
	long insertPizza(long idCommande, long idPizza);
		
	@SqlQuery("SELECT * FROM commandePizzaAssociation WHERE idCommande = :idCommande AND idPizza = :idPizza")
	@RegisterBeanMapper(CommandeAssociation.class)
	CommandeAssociation findAssociationByIds(long idCommande, long idPizza);
	
	@SqlQuery("SELECT * FROM commandePizzaAssociation WHERE idCommande = :idCommande")
	@RegisterBeanMapper(CommandeAssociation.class)
	List<CommandeAssociation> findAssociationByCommande(long idCommande);
	
	@SqlUpdate("DELETE FROM commandePizzaAssociation WHERE idCommande = :idCommande")
	void removeAssociationByCommande(long idCommande);

}