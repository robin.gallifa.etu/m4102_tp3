## Développement d'une ressource *commandes*

### API et représentation des données

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes | récupère l'ensemble des commandes          | 200 et un tableau de commandes               |
| GET       | /commandes/{id} | récupère la commande d'identifiant id  | 200 et la commande                           |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/name | récupère la nom de famille du client de la commande d'identifiant id  | 200 et le nom de la commande                 |
|           |             |                             | 404 si nom est inconnu                         |
| GET       | /commandes/{id}/pizzas | récupère l'ensemble des pizza commandes de la commande d'identifiant id | 200 et un tableau de pizzas               |
| POST      | /commandes | création d'une commande                     | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la commande existe déjà (même nom)    |
| DELETE    | /commandes/{id} | destruction de la commande d'identifiant id | 204 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |


Une commande comporte uniquement un identifiant, le nom et prénom du client, et la liste des pizzas commandées. 
Sa représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
      "firstName": "Jonathan",
      "name": "Jouarnier",
      "pizzas": [{"name" = "oranaise", "ingredients" = [{"name" = "jambon}, {"name" = "patate"}]}, {"name" = "montagnarde", "ingredients" = []}]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni par la base de données. 
Aussi on aura une représentation JSON qui comporte uniquement la liste de pizzas et le nom et prénom du client :

	{ "firstName": "Jonathan",
      "name": "Jouarnier",
      "pizzas": [{"name" = "oranaise", "ingredients" = [{"name" = "jambon}, {"name" = "patate"}]}, {"name" = "montagnarde", "ingredients" = []}]

